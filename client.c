 /*
    ** client.c -- klient używający gniazd strumieniowych
    */

    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <netdb.h>
    #include <sys/types.h>
    #include <netinet/in.h>
    #include <sys/socket.h>
    #include <stdbool.h>    

    #define PORT 7000 // port, z którym klient będzie się łączył

    #define MAXDATASIZE 100 // maksymalna ilość dancyh, jaką możemy otrzymać na raz

    int main(int argc, char *argv[])
    {
        int sockfd, numbytes,wysyl;  
        char buf[MAXDATASIZE];
        char buf1[MAXDATASIZE];
        struct hostent *he;
        struct sockaddr_in their_addr; // informacja o adresie osoby łączącej się
        
        

        if (argc != 2) {
            fprintf(stderr,"usage: client hostname\n");
            exit(1);
        }

        if ((he=gethostbyname(argv[1])) == NULL) {  // pobierz informacje o hoście
            perror("gethostbyname");
            exit(1);
        }

        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            perror("socket");
            exit(1);
        }

        their_addr.sin_family = AF_INET;    // host byte order 
        their_addr.sin_port = htons(PORT);  // short, network byte order 
        their_addr.sin_addr = *((struct in_addr *)he->h_addr);
        memset(&(their_addr.sin_zero), '\0', 8);  // wyzeruj resztę struktury

        if (connect(sockfd, (struct sockaddr *)&their_addr,
                                              sizeof(struct sockaddr)) == -1) {
            perror("connect");
            exit(1);
        }
      for(;;){    //nieskonczona petla kończąca się po otrzymaniu odpowiedniego komunikatu
         printf("Podaj wiadomosc do wyslania:");
         scanf("%s",buf1);//wpisywanie z klawiatury
         if((wysyl =write(sockfd,buf1,MAXDATASIZE-1))==-1){
         perror("Wysylanie ");
           exit(1);
         }

         if ((numbytes=recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) { //obiór wiadomości
            perror("Odbieranie ");
            exit(1);
         }
         if(strcmp(buf,"Msg\n")==0) break;//sprawdzamy czy otrzymalismy od servera wiadomosc Msg i opuszczamy pętlę
        
         buf[numbytes] = '\0'; //odczytanie i powrót na początek pętli
         printf("Odebrano: %s",buf);
        }
        buf[numbytes] = '\0';  //potrzebne w celu wyswietlenia w przypadku gdy opuscilismy pętlę
        printf("Odebrano: %s",buf);
        close(sockfd);

        return 0;
    } 