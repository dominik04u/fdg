   /*
    ** server.c -- serwer używający gniazd strumieniowych
    */

    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <signal.h>
    #include <time.h>

    #define MYPORT 7000    // port, z którym będą się łączyli użytkownicy
    #define MAXDATASIZE 100
    #define BACKLOG 10     // jak dużo możę być oczekujących połączeń w kolejce

    void sigchld_handler(int s)
    {
        while(wait(NULL) > 0);
    }

    int main(void)
    {
        int sock_fd, new_fd,info,por;  // nasłuchuj na sock_fd, nowe połaczenia na new_fd
        struct sockaddr_in my_addr;    // informacja o moim adresie
        struct sockaddr_in their_addr; // informacja o adresie osoby łączącej się
        int sin_size;
        struct sigaction sa;
        int yes=1;
        char buf[MAXDATASIZE];
        time_t t;
        time(&t);
       

        if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            perror("socket");
            exit(1);
        }

        if (setsockopt(sock_fd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
            perror("setsockopt");
            exit(1);
        }
        
        my_addr.sin_family = AF_INET;         // host byte order
        my_addr.sin_port = htons(MYPORT);     // short, network byte order
        my_addr.sin_addr.s_addr = INADDR_ANY; // uzupełnij moim adresem IP
        memset(&(my_addr.sin_zero), '\0', 8); // wyzeruj resztę struktury

        if (bind(sock_fd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))
                                                                       == -1) {
            perror("bind");
            exit(1);
        }

        if (listen(sock_fd, BACKLOG) == -1) {
            perror("listen");
            exit(1);
        }

        sa.sa_handler = sigchld_handler; // zbierz martwe procesy
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_RESTART;
        if (sigaction(SIGCHLD, &sa, NULL) == -1) {
            perror("sigaction");
            exit(1);
        }

        while(1) {  // głowna pętla accept()
            sin_size = sizeof(struct sockaddr_in);
            if ((new_fd = accept(sock_fd, (struct sockaddr *)&their_addr,
                                                           &sin_size)) == -1) {
                perror("accept");
                continue;
            }
            printf("server: got connection from %s\n",
                                               inet_ntoa(their_addr.sin_addr));
          if (!fork()) { // to jest proces-dziecko
                        
           while(1){ //pętla do stałej komunikacji z klientem
            printf("Czekam \n");
            if((info=  read( new_fd,buf,MAXDATASIZE-1))==-1)
	    {//odbieranie wiadomosci
             perror("Odbieranie ");
             exit(1);
            }
            buf[info] = '\0';
            printf("Odebrano: %s \n",buf);
            if(strcmp( buf, "1004" )==0){  //warunki sprawdzajace otzymana wiadomosc
             if(write( new_fd, ctime(&t), 25 ) == - 1 )
               perror( "Wysylanie" );
            }else if(strcmp(buf,"koniec")==0){
              if(send( new_fd, "Msg\n", 5, 0) == - 1)
                 perror("wysylanie");
              break;//jesli klient wyslal koniec opuszamy pętle while
            }else{
              if(write( new_fd, "Brak polecenia\n", 16 ) == - 1 )
                 perror( "Wysylanie" );
            }

           }  
               
            }
            close(new_fd);  // rodzic nie potrzebuje tego
        }

        return 0;
    } 